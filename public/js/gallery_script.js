function getimages(dir){
    const imagearray = ['a','b','c'];
    imagearray.length = 0;

    var fileextension = ".jpg";
    $.ajax({
        url: dir,
        success: function (data) {
            $(data).find("a:contains(" + fileextension + ")").each(function () {
                var filename = this.href.replace(window.location.host, "").replace("http://", "");
                filename="https://perfect10improv.org" + filename;
                console.log("this.href: "+this.href);
                console.log("filename: "+filename);
                imagearray.push(filename);
            });
        },
        async: false    // THIS IS REALLY BAD PRACTICE IT WILL BLOCK THE WEBSITE FROM PROCEDEING UNTIL IT'S FINISHED
                        // BUT I DON'T KNOW HOW ELSE TO DO THIS I'M BAD AT PROGRAMMING
    });

    return imagearray;
}

document.addEventListener("DOMContentLoaded", function () {
    fetch('./gallery_pics/gallery_info.json')
        .then(response => response.json())
        .then(gallery_info => {
            const dataDisplay = document.getElementById("dataDisplay");

            // Create HTML elements to display the JSON data
            // const nameElement = document.createElement("p");
            // nameElement.textContent = "Title: " + gallery_info.gallery[0].title;

            // Append the elements to the "dataDisplay" div
            //dataDisplay.appendChild(nameElement);
            //var tempArray = ['./gallery_pics/1.jpg','./gallery_pics/2.jpg'];
            for(var i=0; i<gallery_info.gallery.length; i++){

                var newPath = './gallery_pics/' + gallery_info.gallery[i].folder;
                //dataDisplay.innerHTML+=('<h3>' + newPath + '</h3>');

                //for pictures in path..
                console.log("---- i="+i);
                console.log("newPath: "+newPath);
                //console.log(getimages(newPath));
                //console.log(getimages(newPath).length);
                
                const tempArray = getimages(newPath);
                console.log(tempArray);
                //dataDisplay.innerHTML+=("Hella: " + tempArray);
                // tempArray.forEach(function(item, index) { 
                //     console.log(index+","+item+" photo "+tempArray[index]);
                // }); 

                    // TO FUTURE COLBY:
                    // I reckon a couple path's forward
                    // 1. Use something other than innerHTML, it doesn't seem to play nice if it's split up
                    // 2. Concantonate all the individual bits you need in to one long string(?) thing and then innerHTML that string(?)
                    // 3. Go to bed (this is the one I'm doing now, good luck future me)

                var bigCont = ''; //This is a wild way to do this. What the hell am I thinking
                

                bigCont+='<section class="u-align-center u-clearfix u-section-2" id="carousel_add5">'
                bigCont+='<div class="u-clearfix u-sheet u-sheet-1">'
                bigCont+='<h2 class="u-custom-font u-text u-text-default u-text-1">' + gallery_info.gallery[i].title + '</h2>'
                bigCont+='<div class="u-expanded-width u-gallery u-layout-grid u-lightbox u-show-text-on-hover u-gallery-1" ">'
                bigCont+='<div class="u-gallery-inner u-gallery-inner-1">'

                tempArray.forEach(function(item, index) { 
                    //console.log(index+","+item);
                    bigCont+='<div class="u-effect-fade u-gallery-item" ">'
                    bigCont+='<div class="u-back-slide">'
                    console.log("item.replace: "+item.replace("/public",""));
                    bigCont+='<img class="u-back-image u-expanded" src="' + item.replace("/public","") + '">'
                    bigCont+='</div>'
                    
                    bigCont+='<div class="u-over-slide u-shading u-over-slide-'+(index+1)+'"></div>'
                    bigCont+='</div>'

                }); 
                

                bigCont+='</div>'
                bigCont+='</div>'
                bigCont+='</section>'
                dataDisplay.innerHTML+=(bigCont);

                // dataDisplay.innerHTML+=(
                //     '<section class="u-align-center u-clearfix u-section-2" id="carousel_add5">'
                //     + '<div class="u-clearfix u-sheet u-sheet-1">'
                //     + '<h2 class="u-custom-font u-text u-text-default u-text-1">' + gallery_info.gallery[i].title + '</h2>'
                //     + '<div class="u-expanded-width u-gallery u-layout-grid u-lightbox u-show-text-on-hover u-gallery-1">'
                //     + '<div class="u-gallery-inner u-gallery-inner-1">'
                // );

                // tempArray.forEach(function(item, index) { 
                //     console.log(index+","+item+" photo "+tempArray[index]);
                //     dataDisplay.innerHTML+=(
                //         + '<div class="u-effect-fade u-gallery-item">'
                //         + '<div class="u-back-slide">'
    
                //         + '<img class="u-back-image u-expanded" src="' + tempArray[index] + '">'
                        
                //         + '</div>'
                //         + '<div class="u-over-slide u-shading u-over-slide-1"></div>'
                //         + '</div>'
                //     );
                // }); 

                
                
                // dataDisplay.innerHTML+=(
                //     + '</div>'
                //     + '</div>'
                //     + '</div>'
                //     + '</section>'
                // );
            }

        })
        .catch(error => console.error("Error fetching JSON data:", error));
});
