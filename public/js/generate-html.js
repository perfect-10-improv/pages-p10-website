const fs = require('fs');
const path = require('path');

const eventsDir = path.join(__dirname, '../events');
const outputDir = path.join(__dirname, '../');

if (!fs.existsSync(outputDir)){
    fs.mkdirSync(outputDir);
}

const generateHTML = () => {
    const events = fs.readdirSync(eventsDir).map(folder => {
        const imagesDir = path.join(eventsDir, folder);
        const images = fs.readdirSync(imagesDir).map(file => path.join('events', folder, file));
        return { title: folder, images };
    });

    const html = `
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Event Carousels</title>
            <style>
                .event {
                    margin: 20px 0;
                }
                .event-title {
                    font-size: 24px;
                    margin-bottom: 10px;
                }
                .carousel {
                    display: flex;
                    overflow-x: auto;
                    scroll-behavior: smooth;
                }
                .carousel img {
                    width: 200px;
                    height: 150px;
                    margin-right: 10px;
                }
            </style>
        </head>
        <body>
            <div id="events">
                ${events.map(event => `
                    <div class="event">
                        <h2 class="event-title">${event.title}</h2>
                        <div class="carousel">
                            ${event.images.map(image => `<img src="${image}" alt="${event.title} image">`).join('')}
                        </div>
                    </div>
                `).join('')}
            </div>
        </body>
        </html>
    `;

    fs.writeFileSync(path.join(outputDir, 'newgallery.html'), html);
};

generateHTML();
